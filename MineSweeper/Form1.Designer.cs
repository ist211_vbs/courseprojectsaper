﻿namespace MineSweeper
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.рівеньСкладностіToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.новачок99ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.середнійToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.профіToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.новаяИграToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.новаяИграToolStripMenuItem,
            this.справкаToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(11, 4, 0, 4);
            this.menuStrip1.Size = new System.Drawing.Size(890, 32);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.рівеньСкладностіToolStripMenuItem,
            this.новачок99ToolStripMenuItem,
            this.середнійToolStripMenuItem,
            this.профіToolStripMenuItem});
            this.toolStripMenuItem1.Font = new System.Drawing.Font("Berlin Sans FB", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(46, 24);
            this.toolStripMenuItem1.Text = "Гра";
            // 
            // рівеньСкладностіToolStripMenuItem
            // 
            this.рівеньСкладностіToolStripMenuItem.Name = "рівеньСкладностіToolStripMenuItem";
            this.рівеньСкладностіToolStripMenuItem.Size = new System.Drawing.Size(276, 26);
            this.рівеньСкладностіToolStripMenuItem.Text = "Рівень складності:";
            // 
            // новачок99ToolStripMenuItem
            // 
            this.новачок99ToolStripMenuItem.Name = "новачок99ToolStripMenuItem";
            this.новачок99ToolStripMenuItem.Size = new System.Drawing.Size(276, 26);
            this.новачок99ToolStripMenuItem.Text = "Новачок (9 * 9) 10 бомб";
            this.новачок99ToolStripMenuItem.Click += new System.EventHandler(this.новичок99ToolStripMenuItem_Click);
            // 
            // середнійToolStripMenuItem
            // 
            this.середнійToolStripMenuItem.Name = "середнійToolStripMenuItem";
            this.середнійToolStripMenuItem.Size = new System.Drawing.Size(276, 26);
            this.середнійToolStripMenuItem.Text = "Середній (16 * 16) 40 бомб";
            this.середнійToolStripMenuItem.Click += new System.EventHandler(this.среднийToolStripMenuItem_Click);
            // 
            // профіToolStripMenuItem
            // 
            this.профіToolStripMenuItem.Name = "профіToolStripMenuItem";
            this.профіToolStripMenuItem.Size = new System.Drawing.Size(276, 26);
            this.профіToolStripMenuItem.Text = "Профі (16 * 30) 99 бомб";
            this.профіToolStripMenuItem.Click += new System.EventHandler(this.профиToolStripMenuItem_Click);
            // 
            // новаяИграToolStripMenuItem
            // 
            this.новаяИграToolStripMenuItem.Font = new System.Drawing.Font("Berlin Sans FB", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.новаяИграToolStripMenuItem.Name = "новаяИграToolStripMenuItem";
            this.новаяИграToolStripMenuItem.Size = new System.Drawing.Size(84, 24);
            this.новаяИграToolStripMenuItem.Text = "Нова гра";
            this.новаяИграToolStripMenuItem.Click += new System.EventHandler(this.новаяИграToolStripMenuItem_Click_1);
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.Font = new System.Drawing.Font("Berlin Sans FB", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(79, 24);
            this.справкаToolStripMenuItem.Text = "Справка";
            this.справкаToolStripMenuItem.Click += new System.EventHandler(this.справкаToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 625);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "MineSweeper";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem новачок99ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem середнійToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem профіToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem новаяИграToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem рівеньСкладностіToolStripMenuItem;
    }
}

